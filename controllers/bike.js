var Bike = require("../models/bike");

exports.bike_list = (req, res) => {
  Bike.allBikes((err, bikes) => {
    res.render("bikes/index", { bikes: bikes });
  });
};

exports.bike_create_get = (req, res) => {
  res.render("bikes/create");
};

exports.bike_create_post = (req, res) => {
  console.log("CREATE POST");
  console.log(req.body);
  const { code, color, model, lat, lng } = req.body;
  //var bike = new Bike(code, color, model);
  //bike.location = [lat, lng];

  var bike = {
    code: code,
    color: color,
    model: model,
    location: [lat, lng],
  };

  Bike.add(bike, (err) => {
    if (err) return console.log(err);
    console.log("bike creada");
    res.redirect("/bikes");
  });
};

exports.bike_update_get = (req, res) => {
  //var bike = Bike.findByCode(req.params.id);
  Bike.findByCode(req.params.id, (err, bike) => {
    console.log("THE BIKE");
    console.log(bike);
    res.render("bikes/update", { bike });
  });
};

exports.bike_update_post = (req, res) => {
  const { code, color, model, lat, lng } = req.body;
  Bike.findByCode(code, (err, bike) => {
    bike.color = color;
    bike.model = model;
    bike.location = [lat, lng];

    Bike.updateOne(bike, (err, bike) => {
      if (err) console.log(err);
      res.redirect("/bikes");
    });
  });
};

exports.bike_del_post = (req, res) => {
  console.log(req.params.id);
  Bike.removeByCode(req.params.id, (err, result) => {
    if (err) console.log(err);
    res.redirect("/bikes");
  });
};
