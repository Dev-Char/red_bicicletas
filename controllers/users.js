var User = require("../models/user");

module.exports = {
  // listar
  list: (req, res, next) => {
    User.find({}, (err, users) => {
      console.log(users);
      res.render("users/index", { users: users ? users : [] });
    });
  },

  // crear get
  create_get: (req, res, next) => {
    res.render("users/create", { errs: {}, user: new User() });
  },

  // crear post
  create: (req, res, next) => {
    console.log(req.body);
    const { name, password, confirm_password, email } = req.body;

    if (password != confirm_password) {
      res.render("users/create", {
        errs: { confirm_password: { message: "Las contraseña no coiciden" } },
        user: new User({ name: name, email: email }),
      });
      return;
    }

    User.create({ name: name, email: email, password: password }, function (
      err,
      newUser
    ) {
      if (err) {
        console.log("errror:" + err);
        res.render("users/create", {
          errs: err.errors,
          user: new User({ name: name, email: email }),
        });
      } else {
        newUser.send_email_welcome();
        res.redirect("/users");
      }
    });
  },

  // actualizar get
  update_get: (req, res, next) => {
    User.findById(req.params.id, (err, user) => {
      res.render("users/update", { errs: {}, user: user });
    });
  },

  // actualizar post
  update: (req, res, next) => {
    const { name, email } = req.body;
    var update_values = { name: name };
    User.findByIdAndUpdate(req.params.id, update_values, (err, user) => {
      console.log("actualizaaaa");
      if (err) {
        console.log(err);
        res.render("users/update", {
          errs: err.errors,
          user: new User({ name: name, email: email }),
        });
      } else {
        res.redirect("/users");
        return;
      }
    });
  },

  // eliminar post
  delete: (req, res) => {
    User.findByIdAndDelete(req.body.id, (err) => {
      if (err) {
        next(err);
      } else {
        res.redirect("/users");
      }
    });
  },
};
