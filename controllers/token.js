var User = require("../models/user");
var Token = require("../models/token");

module.exports = {
  confirmationGet: function (req, res, next) {
    Token.findOne({ token: req.params.token }, function (err, token) {
      if (!token) {
        return res.status(4000).send({
          type: "not-verified",
          msg:
            "No se encotro usuario con este token. Quiza haya expirado y debeas solicitar uno nuevamente",
        });
      }
      User.findById(token._userId, function (err, user) {
        if (!user) {
          return res.status(400).send({ msg: "No se encontro usuario" });
        }

        if (user.verified) return res.redirect("/users");
        user.verified = true;

        user.save(function (err) {
          if (err) {
            return res.status(500).send({ msg: err.message });
          }

          res.redirect("/");
        });
      });
    });
  },
};
