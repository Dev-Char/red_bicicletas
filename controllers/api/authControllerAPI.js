const User = require("../../models/user");
const bcrypt = require("bcrypt");
const jwt = require("jsonwebtoken");

module.exports = {
  authenticate: function (req, res, next) {
    const { email, password } = req.body;
    User.findOne({ email: email }, (err, userInfo) => {
      if (err) {
        next(err);
      } else {
        if (userInfo === null) {
          return res.status(401).json({
            status: "error",
            message: "Invalido Email/Password",
            data: null,
          });
        }
        if (
          userInfo !== null &&
          bcrypt.compareSync(password, userInfo.password)
        ) {
          // generamos token jwt
          console.log("*2*");
          const token = jwt.sign(
            { id: userInfo._id },
            req.app.get("secretKey"),
            {
              expiresIn: "7d",
            }
          );
          console.log("*3*");
          res.status(200).json({
            message: "Usuario encontrado",
            data: { user: userInfo, token: token },
          });
        } else {
          console.log("*4*");
          res.status(401).json({
            status: "error",
            message: "Invalido Email/Password",
            data: null,
          });
        }
      }
    });
  },

  forgotPassword: function (req, res, next) {
    const { email, password } = req.body;
    User.findOne({ email: email }, (err, user) => {
      if (!user) {
        res.status(401).json({
          status: "error",
          message: "No existe el usuario",
          data: null,
        });
      }
      user.resetPassword((err) => {
        if (err) {
          next(err);
        }
        res.status(200).json({
          message: "Se envio un email para restablecer la contraseña",
          data: null,
        });
      });
    });
  },

  authFacebookToken: function (req, res, next) {
    if (req.user) {
      req.user
        .save()
        .then(() => {
          console.log("USERRRR");
          console.log(user);
          const token = jwt.sign(
            { id: req.user.id },
            req.app.get("secretKey"),
            { expiresIn: "7d" }
          );
          console.log("BIEN:" + token);
          res.status(200).json({
            message: "Un usuario encontrado o creado",
            data: { user: req.user, token: token },
          });
        })
        .catch((err) => {
          console.log("error controller" + err);
          res.status(500).json({ message: err.message });
        });
    } else {
      res.status(401);
    }
  },
};
