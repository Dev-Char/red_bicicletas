var User = require("../../models/user");

exports.users_list = function (req, res) {
  User.find({}, (err, users) => {
    res.status(200).json({ users: users });
  });
};

exports.users_create = function (req, res) {
  const { name, email, password } = req.body;
  var user = new User({ name: name, email: email, password: password });

  user.save((err) => {
    if (err) console.log("Hay un error");
    res.status(200).json(user);
  });
};

exports.user_reserve = function (req, res) {
  const { id, bike_id, from, to } = req.body;
  User.findById(id, (err, user) => {
    console.log(user);
    user.reserve(bike_id, from, to, (err) => {
      if (err) console.log(err);
      console.log("reserva!");
      res.status(200).send();
    });
  });
};
