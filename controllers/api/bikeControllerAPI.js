var Bike = require("../../models/bike");

exports.bike_list = function (req, res) {
  //Bike.find({}, (err, bikes) => {
  Bike.allBikes((err, bikes) => {
    res.status(200).json({ bikes: bikes });
  });
};

exports.bike_create = function (req, res) {
  console.log(req.body);
  const { code, color, model, lat, lng } = req.body;
  var bike = new Bike({ code, color, model });
  bike.location = [lat, lng];
  /*
  Bike.add(bike, (err, Newbike) => {
    res.status(200).json({
      bike: Newbike,
    });
  });
  */
  bike.save((err) => {
    if (err) console.log("Hay un error");
    res.status(200).json(bike);
  });
};

exports.bike_update = (req, res) => {
  const { code, color, model, lat, lng } = req.body;

  Bike.findByCode(code, (err, aBike) => {
    if (err) console.log(err);
    if (!aBike) {
      res.status(500).json({ message: "No se encontro", bike: aBike });
    } else {
      aBike.color = color;
      aBike.model = model;
      aBike.location = [lat, lng];
    }
    aBike.save((err) => {
      if (err) console.log("Hay un error");
      res.status(203).json({ bike: aBike });
    });
  });
};

exports.bike_delete = function (req, res) {
  Bike.removeByCode(req.body.code, (err, result) => {
    if (err) console.log("EERRROOR:" + err);
    res.status(204).send(result);
  });
};
