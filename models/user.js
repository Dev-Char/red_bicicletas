var mongoose = require("mongoose");
var Reservation = require("./reservation");
var Schema = mongoose.Schema;
var bcrypt = require("bcrypt");
var crypto = require("crypto");
var uniqueValidator = require("mongoose-unique-validator");

const saltRounds = 10; // aleatoridad a la encriptacion
const Token = require("./token");
const mailer = require("../mailer/mailer");

// validacion de Email
const validateEmail = (email) => {
  const re = /^([\w-\.]+@([\w-]+\.)+[\w-]{2,4})?$/;
  return re.test(email);
};

// Usuario
var userSchema = new Schema({
  name: {
    type: String,
    trim: true,
    required: [true, "El nombre es obligatorio"],
  },
  email: {
    type: String,
    trim: true,
    required: [true, "El email es obligatorio"],
    lowercase: true,
    validate: [validateEmail, "Ingrese un email valido"],
    match: [/^([\w-\.]+@([\w-]+\.)+[\w-]{2,4})?$/],
    unique: true,
  },
  password: {
    type: String,
    required: [true, "El password es obligatorio"],
  },
  passwordResetToken: String,
  passwordResetTokenExpires: Date,
  verified: {
    type: Boolean,
    default: false,
  },
  googleId: String,
  facebookId: String,
});

// agregar como plugin
userSchema.plugin(uniqueValidator, { message: "El {PATH} ya esta registrado" });

// antes del save se ejecuta esta funcion
userSchema.pre("save", function (next) {
  if (this.isModified("password")) {
    this.password = bcrypt.hashSync(this.password, saltRounds);
  }
  next();
});

// valida contraseña
userSchema.methods.validPassword = function (pass) {
  return bcrypt.compareSync(pass, this.password);
};

// hacer una reservar
userSchema.methods.reserve = function (bikeId, from, to, cb) {
  var reservation = new Reservation({ user: this._id, bike: bikeId, from, to });

  reservation.save(cb);
};

userSchema.methods.send_email_welcome = function (cb) {
  const token = new Token({
    _userId: this._id,
    token: crypto.randomBytes(16).toString("hex"),
  });
  const email_destination = this.email;
  token.save(function (err) {
    if (err) {
      return console.log(err.message);
    }

    let mailOptions = {
      from: "no-reply@redBicicletas.com>",
      to: email_destination,
      subject: "Verificacion de cuenta.",
      text:
        "Hola!\n" +
        " para verificar la cuenta haga clicke en este link: \n" +
        "http://localhost:3000" +
        "/token/confirmation/" +
        token.token +
        ".\n",
      //html: "<p><b>Hello</b> to myself!</p>",
    };

    mailer.sendMail(mailOptions, (err, info) => {
      if (err) {
        return console.log("Error    de ethereal" + err.message);
      }

      console.log(
        "La verificacion de email fue enviada a " + email_destination
      );
    });
  });
};

userSchema.methods.resetPassword = function (cb) {
  const token = new Token({
    _userId: this._id,
    token: crypto.randomBytes(16).toString("hex"),
  });
  const email_destination = this.email;
  token.save(function (err) {
    if (err) {
      return cb(err);
    }

    let mailOptions = {
      from: "no-reply@redBicicletas.com>",
      to: email_destination,
      subject: "Reseteo de contraseña de la cuenta.",
      text:
        "Hola!\n" +
        " para resetear la contraseña haga clicke en este link: \n" +
        "http://localhost:3000" +
        "/resetPassword/" +
        token.token +
        ".\n",
      //html: "<p><b>Hello</b> to myself!</p>",
    };

    mailer.sendMail(mailOptions, (err, info) => {
      if (err) {
        return console.log("Error de ethereal" + err.message);
      }

      console.log(
        "El reseteo de contraseña fue enviada a " + email_destination
      );
    });
    cb(null);
  });
};

userSchema.statics.findOneOrCreateByGoogle = function findOneOrCreate(
  condition,
  callback
) {
  const { id, displayName, emails } = condition;
  const self = this;
  console.log(condition);
  self.findOne(
    {
      $or: [{ googleId: id }, { email: emails[0].value }],
    },
    (err, result) => {
      if (result) {
        callback(err, result);
      } else {
        console.log("---- CONDITION ----");
        console.log(condition);
        let values = {};
        values.googleId = id;
        values.email = emails[0].value;
        values.name = displayName || "Sin Nombre";
        values.verified = true;
        //values.password = condition._json.etag;
        values.password = crypto.randomBytes(16).toString("hex");
        console.log("-- VALUES --");
        console.log(values);
        self.create(values, (err, result) => {
          if (err) {
            console.log(err);
          }
          return callback(err, result);
        });
      }
    }
  );
};

userSchema.statics.findOneOrCreateByFacebook = function findOneOrCreate(
  condition,
  callback
) {
  const { id, displayName, emails } = condition;
  const self = this;
  console.log("***");
  console.log(condition);
  console.log("****");
  this.findOne(
    {
      $or: [{ facebookId: id }, { email: emails[0].value }],
    },
    (err, result) => {
      if (result) {
        console.log("EXISTE");
        callback(null, result);
      } else {
        if (err) return callback(err);

        console.log("NO EXISTE");

        let values = {};
        values.facebookId = id;
        values.email = emails[0].value;
        values.name = displayName || "Sin Nombre";
        values.verified = true;
        values.password = crypto.randomBytes(16).toString("hex");

        self.create(values, (err, result) => {
          if (err) {
            console.log("AQUI");
            console.log(err);
          } else {
            console.log("NO AQUI");
            return callback(null, result);
          }
        });
      }
    }
  );
};

module.exports = mongoose.model("User", userSchema);
