var mongoose = require("mongoose");
var Schema = mongoose.Schema;

var bikeSchema = new Schema({
  code: Number,
  color: String,
  model: String,
  location: {
    type: [Number],
    index: { type: "2dsphere", sparse: true },
  },
});

bikeSchema.statics.createInstance = function (code, color, model, location) {
  return new this({
    code: code,
    color: color,
    model: model,
    location: location,
  });
};

bikeSchema.methods.toString = function () {
  return "code: " + this.id + " | color:  " + this.color;
};

// .statics. se pase directo al modelo

bikeSchema.statics.allBikes = function (cb) {
  return this.find({}, cb);
};

bikeSchema.statics.add = function (bike, cb) {
  console.log("BIKE ");
  //console.log(bike);
  this.create(bike, cb);
};

bikeSchema.statics.findByCode = function (code, cb) {
  return this.findOne({ code: code }, cb); // trae el primero por el codigo
};

bikeSchema.statics.removeByCode = function (code, cb) {
  return this.deleteOne({ code: code }, cb); // eliminar el primero
};

module.exports = mongoose.model("Bike", bikeSchema);

//

/*
var Bike = function (id, color, model, location) {
  this.id = id;
  this.color = color;
  this.model = model;
  this.location = location;
};

Bike.allBikes = [];

// agregar
Bike.add = (abike) => {
  Bike.allBikes.push(abike);
};

// buscar bici
Bike.findById = (id) => {
  var aBike = Bike.allBikes.find((x) => x.id == id);
  if (aBike) {
    return aBike;
  } else {
    throw new Error(`No existe una bicicleta con ese id: ${id}`);
  }
};

// eliminar
Bike.removeById = (id) => {
  //var aBike = Bike.findById(id); // check error

  for (var i = 0; i < Bike.allBikes.length; i++) {
    if (Bike.allBikes[i].id == id) {
      Bike.allBikes.splice(i, 1);
      break;
    }
  }
};

var a = new Bike(1, "red", "urban", [-38.017059, -57.7406706]);
var b = new Bike(2, "green", "urban", [-38.045903, -57.573197]);

Bike.add(a);
Bike.add(b);

/*
Bike.prototype.toString = function () {
  return "id: " + this.id + " | color:  " + this.color;
};


module.exports = Bike;
*/
