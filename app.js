require("dotenv").config();
var createError = require("http-errors");
var express = require("express");
var path = require("path");
var cookieParser = require("cookie-parser");
var logger = require("morgan");
const passport = require("./config/passport");
const session = require("express-session");
const jwt = require("jsonwebtoken");

const User = require("./models/user");
const Token = require("./models/token");

var indexRouter = require("./routes/index");
var bikesAPIRouter = require("./routes/api/bikes");
var userAPIRouter = require("./routes/api/user");
var authAPIRouter = require("./routes/api/auth");

var bikesRouter = require("./routes/bikes");
var usersRouter = require("./routes/users");
var tokenRouter = require("./routes/token");
require("newrelic");

const authController = require("./controllers/api/authControllerAPI");

const MongoDBStore = require("connect-mongodb-session")(session);

let store;
if (process.env.NODE_ENV === "development") {
  store = new session.MemoryStore();
} else {
  store = new MongoDBStore({
    uri: process.env.MONGO_URI,
    collection: "sessions",
  });
  store.on("error", (err) => {
    assert.ifError(err);
    assert.ok(false);
  });
}

var app = express();

app.set("secretKey", "jwt_pwd_!!223344");

app.use(
  session({
    cookie: { maxAge: 240 * 60 * 60 * 1000 },
    store: store,
    saveUninitialized: true,
    resave: true,
    secret: "...",
  })
);

var mongoose = require("mongoose");
const { assert } = require("console");

var mongoDB = process.env.MONGO_URI;

mongoose.connect(mongoDB, { useNewUrlParser: true, useUnifiedTopology: true });
mongoose.set("useCreateIndex", true);
mongoose.Promise = global.Promise;

var db = mongoose.connection;
db.on("error", console.error.bind(console, "MongoDB connection error:"));

//db.once("open", function () {});

// view engine setup
app.set("views", path.join(__dirname, "views"));
app.set("view engine", "pug");

app.use(logger("dev"));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
// session
app.use(passport.initialize());
app.use(passport.session());
app.use(express.static(path.join(__dirname, "public")));

app.use("/", indexRouter);

app.get("/login", (req, res) => {
  res.render("session/login");
});
app.post("/login", (req, res, next) => {
  passport.authenticate("local", (err, user, info) => {
    if (err) return next(err);
    if (!user) return res.render("session/login", { info });
    req.login(user, function (err) {
      console.log("ok");
      if (err) return next(err);
      return res.redirect("/");
    });
  })(req, res, next);
});

app.get("/logout", (req, res) => {
  req.logout();
  res.redirect("/");
});

app.get("/forgotPassword", (req, res) => {
  res.render("session/forgotPassword");
});

app.post("/forgotPassword", (req, res) => {
  User.findOne({ email: req.body.email }, (err, user) => {
    if (!user)
      return res.render("session/forgotPassword", {
        info: { message: "No existe el email" },
      });

    user.resetPassword((err) => {
      if (err) return next(err);
      console.log("session/forgotPasswordMessage");
    });
    res.render("session/forgotPasswordMessage");
  });
});

app.get("/resetPassword/:token", (req, res, next) => {
  Token.findOne({ token: req.params.token }, (err, token) => {
    if (!token)
      return res.status(400).send({
        type: "not-verified",
        msg:
          "No existe usuario asociado a ese token, verifique que su token no haya expirado",
      });

    User.findById(token._userId, (err, user) => {
      if (!user)
        return res
          .status(400)
          .send({ msg: "No existe usuario asociado a ese token" });
      res.render("session/resetPassword", { errors: {}, user: user });
    });
  });
});

app.post("/resetPassword", (req, res) => {
  const { password, email, confirm_password } = req.body;
  if (password != confirm_password) {
    res.render("session/resetPassword", {
      errors: {
        confirm_password: {
          message: "No coinciden con la contraseña ingresada",
        },
      },
      user: new User({ email: email }),
    });
    return;
  }
  User.findOne({ email: email }, (err, user) => {
    (user.password = password),
      user.save((err) => {
        if (err) {
          res.render("session/resetPassword", {
            errors: err.errors,
            user: new User({ email: email }),
          });
        } else {
          res.redirect("/login");
        }
      });
  });
});

app.get(
  "/auth/google",
  passport.authenticate("google", {
    scope: [
      "https://www.googleapis.com/auth/plus.login",
      "https://www.googleapis.com/auth/plus.profile.emails.read",
      "profile",
      "email",
    ],
  })
);

app.get(
  "/auth/google/callback",
  passport.authenticate("google", {
    successRedirect: "/",
    failureRedirect: "/error",
  })
);

// FACEBOOK ** use los metodos que recomendian en la web de PASSPORT

app.get(
  "/auth/facebook",
  passport.authenticate("facebook", {
    authType: "rerequest",
    scope: ["email"],
  }),
  authController.authFacebookToken
);

app.get(
  "/auth/facebook/callback",
  passport.authenticate("facebook", {
    failureRedirect: "/error",
  }),
  function (req, res) {
    // Successful authentication, redirect home.
    res.redirect("/");
  }
);

app.use("/users", usersRouter);
app.use("/bikes", loggedIn, bikesRouter);
app.use("/token", tokenRouter);
app.use("/api/bikes", validUser, bikesAPIRouter);
app.use("/api/users", userAPIRouter);
app.use("/api/auth", authAPIRouter);

app.use("/privacy_policy", (req, res) => {
  res.sendFile("public/privacy_policy.html");
});

app.use("/googleaad86637ed886aee", (req, res) => {
  res.sendFile("googleaad86637ed886aee.html");
});

// catch 404 and forward to error handler
app.use(function (req, res, next) {
  next(createError(404));
});

// error handler
app.use(function (err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get("env") === "development" ? err : {};

  // render the error page
  res.status(err.status || 500);
  res.render("error");
});

function loggedIn(req, res, next) {
  if (req.user) {
    next();
  } else {
    console.log("usuario no logueado");
    res.redirect("/login");
  }
}

function validUser(req, res, next) {
  jwt.verify(req.headers["x-access-token"], req.app.get("secretKey"), function (
    err,
    decoded
  ) {
    if (err) {
      res.json({ status: "error", message: err.message, data: null });
    } else {
      req.body.userId = decoded.id;
      console.log("jwt verificado: " + decoded);
      next();
    }
  });
}

module.exports = app;
