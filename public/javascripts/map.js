var mymap = L.map("main_map").setView([-38.017059, -57.7406706], 13);

L.tileLayer("https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png", {
  attribution:
    'Map data &copy; <a href="https://www.openstreetmap.org/">OpenStreetMap</a> contributors',
  maxZoom: 18,
  titleSize: 512,
}).addTo(mymap);

$.ajax({
  type: "POST",
  dataType: "json",
  url: "api/auth/authenticate",
  data: { email: "char@char.com", password: "123456" },
}).done((data) => {
  $.ajax({
    type: "GET",
    dataType: "json",
    url: "api/bikes",
    beforeSend: (res) => {
      res.setRequestHeader("x-access-token", data.data.token);
    },
  }).done((result) => {
    console.log(result);
    result.bikes.forEach((bike) => {
      L.marker(bike.location, { title: bike.code }).addTo(mymap);
    });
  });
});
