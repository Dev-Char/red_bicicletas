# Curso Backend Node Express Mongo

## Repo **red bicicletas**

#### Gracias por ver mi proyecto

---

> En la carpeta raiz poder ejecutar el proyecto con el comando:

```javascript
npm run dev
```

> En postman podes probar:

```
localhost:3000/api/bikes/
localhost:3000/api/bikes/create
localhost:3000/api/bikes/update
localhost:3000/api/bikes/delete
```

> Testing

Ejecutar todos los test

```
jasmine
```

Ejecutar test especificos

```
jasmine spec/models/bike_test.spec.js
jasmine spec/models/user_test.spec.js
jasmine spec/api/bike_api_test.spec.js

```

reservas

```
localhost:3000/api/users/reserve
```

```javascript
{
    "id": "ID DE USUARIO",
    "bike_id": "ID DE BICICLETA",
    "from": "FECHA DESDE",
    "to": "FECHA HASTA"
}
```

> Auth

```
localhost:3000/api/auth/authenticate
```

> Heroku

LINK APP : [BIKESNET.COM](https://bikesnet.herokuapp.com/)

> usuario de test

```
user: char@char.com
password: 123456
```
