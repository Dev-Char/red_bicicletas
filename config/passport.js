const passport = require("passport");
const LocalStrategy = require("passport-local").Strategy;
const User = require("../models/user");
var GoogleStrategy = require("passport-google-oauth20").Strategy;
//var FacebookTokenStrategy = require("passport-facebook-token"); // no funcionaba bien
var FacebookTokenStrategy = require("passport-facebook").Strategy;

passport.use(
  new LocalStrategy(function (email, password, done) {
    User.findOne({ email: email }, (err, user) => {
      console.log("EERRROR: ");
      console.log(err);
      console.log("USERRRR");
      console.log(user);
      if (err) return done(err);
      if (!user)
        return done(null, false, {
          message: "Email no existe o es incorrecto",
        });
      if (!user.validPassword(password))
        return done(null, false, { message: "Contraseña incorrecta" });
      // con otro if verificacion de email se podria agregar
      return done(null, user);
    });
  })
);

passport.use(
  new GoogleStrategy(
    {
      clientID: process.env.GOOGLE_CLIENT_ID,
      clientSecret: process.env.GOOGLE_CLIENT_SECRET,
      callbackURL: process.env.HOST + "/auth/google/callback",
      proxy: true,
      //passReqToCallback   : true
    },
    function (request, accessToken, refreshToken, profile, done) {
      User.findOneOrCreateByGoogle(profile, function (err, user) {
        return done(err, user);
      });
    }
  )
);

passport.use(
  new FacebookTokenStrategy(
    {
      clientID: process.env.FACEBOOK_ID,
      clientSecret: process.env.FACEBOOK_SECRET,
      callbackURL: process.env.HOST + "/auth/facebook/callback",
      profileFields: ["id", "displayName", "name", "emails"],
    },
    function (accessToken, refreshToken, profile, cb) {
      User.findOneOrCreateByFacebook(profile, function (err, user) {
        return cb(err, user);
      });
    }
  )
);

/*
passport.use(
  new FacebookTokenStrategy(
    {
      clientID: process.env.FACEBOOK_ID,
      clientSecret: process.env.FACEBOOK_SECRET,
    },
    function (accessToken, refreshToken, profile, done) {
      try {
        User.findOneOrCreateByFacebook(profile, function (err, user) {
          if (err) {
            console.log("error facebook: " + err);
          }
          return done(err, user);
        });
      } catch (err2) {
        return done(err2, null);
      }
    }
  )
);
*/
passport.serializeUser(function (user, cb) {
  cb(null, user.id);
});

passport.deserializeUser(function (id, cb) {
  User.findById(id, function (err, user) {
    cb(err, user);
  });
});

module.exports = passport;
