var express = require("express");
var router = express.Router();

var userCotroller = require("../../controllers/api/userControllerAPI");

router.get("/", userCotroller.users_list);
router.post("/create", userCotroller.users_create);
router.post("/reserve", userCotroller.user_reserve);

module.exports = router;
