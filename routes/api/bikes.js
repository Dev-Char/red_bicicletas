var express = require("express");
var router = express.Router();

var bikeCotroller = require("../../controllers/api/bikeControllerAPI");

router.get("/", bikeCotroller.bike_list);
router.post("/create", bikeCotroller.bike_create);
router.put("/update", bikeCotroller.bike_update);
router.delete("/delete", bikeCotroller.bike_delete);

module.exports = router;
