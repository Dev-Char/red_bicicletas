var express = require("express");
var router = express.Router();

var usersCotroller = require("../controllers/users");

router.get("/", usersCotroller.list);
router.get("/create", usersCotroller.create_get);
router.post("/create", usersCotroller.create);
router.get("/:id/update", usersCotroller.update_get);
router.post("/:id/update", usersCotroller.update);
router.post("/:id/delete", usersCotroller.delete);

module.exports = router;
