var express = require("express");
var router = express.Router();

var bikeCotroller = require("../controllers/bike");

router.get("/", bikeCotroller.bike_list);
router.get("/create", bikeCotroller.bike_create_get);
router.post("/create", bikeCotroller.bike_create_post);
router.get("/:id/update", bikeCotroller.bike_update_get);
router.post("/:id/update", bikeCotroller.bike_update_post);
router.post("/:id/delete", bikeCotroller.bike_del_post);

module.exports = router;
