var mongoose = require("mongoose");
var Bike = require("../../models/bike");
var User = require("../../models/user");
var Reservation = require("../../models/reservation");

describe("Testing Users", () => {
  beforeAll((done) => {
    mongoose.connection.close(done);
  });

  // nos conectamos a la db
  beforeEach((done) => {
    var mongoDB = "mongodb://localhost/testdb";
    mongoose.connect(mongoDB, {
      useNewUrlParser: true,
      useUnifiedTopology: true,
      useCreateIndex: true,
    });

    var db = mongoose.connection;
    db.on("error", console.error.bind(console, "MongoDB connection error:"));

    db.once("open", () => {
      console.log("We are connected to test db");
      done();
    });
  });

  // eliminamos todos
  afterEach((done) => {
    Reservation.deleteMany({}, (err, succes) => {
      if (err) console.log(err);
      User.deleteMany({}, (err, success) => {
        if (err) console.log(err);
        Bike.deleteMany({}, (err, success) => {
          if (err) console.log(err);
          done();
          // cierro la conexion sino tira error de timeout
          mongoose.disconnect();
        });
      });
    });
  });

  describe("cuando un Usuario reserve una bici", () => {
    it("debe existir la reserva ", (done) => {
      const user = new User({ name: "Carlos" });
      user.save();
      const bike = new Bike({ code: 2, color: "gris", model: "montaña" });
      bike.save();

      var today = new Date();
      var tomorrow = new Date();
      tomorrow.setDate(today.getDate() + 1);

      user.reserve(bike.id, today, tomorrow, (err, reservation) => {
        Reservation.find({})
          .populate("bike")
          .populate("user")
          .exec(function (err, reservations) {
            expect(reservations.length).toBe(1);
            expect(reservations[0].daysReserve()).toBe(2);
            expect(reservations[0].bike.code).toBe(2);
            expect(reservations[0].user.name).toBe(user.name);
            console.log("- Reserva hecha OK!");
            done();
          });
      });
    });
  });
});
