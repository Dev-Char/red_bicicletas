var Bike = require("../../models/bike");
var mongoose = require("mongoose");

describe("Testing Bikes", () => {
  beforeAll((done) => {
    mongoose.connection.close(done);
  });

  // nos conectamos a la db
  beforeEach((done) => {
    var mongoDB = "mongodb://localhost/testdb";
    mongoose.connect(mongoDB, {
      useNewUrlParser: true,
      useUnifiedTopology: true,
      useCreateIndex: true,
    });
    var db = mongoose.connection;
    db.on("error", console.error.bind(console, "MongoDB connection error:"));

    db.once("open", () => {
      console.log("We are connected to test db");
      done();
    });
  });

  // eliminamos todos
  afterEach((done) => {
    Bike.deleteMany({}, (err, succes) => {
      if (err) console.log(err);
      done();
      // cierro la conexion sino tira error de timeout
      mongoose.disconnect();
    });
  });

  describe("Bike.createInstance", () => {
    it("crea un instancia de bicleta", () => {
      var bike = Bike.createInstance(1, "verde", "montaña", [-34.5, -54.9]);

      expect(bike.code).toBe(1);
      expect(bike.color).toBe("verde");
      expect(bike.model).toBe("montaña");
      expect(bike.location[0]).toBe(-34.5);
      expect(bike.location[1]).toBe(-54.9);
      console.log("- Bike.createInstance OK!");
    });
  });

  describe("Bike.allBikes", () => {
    it("comienza vacia", (done) => {
      Bike.allBikes((err, bikes) => {
        expect(bikes.length).toBe(0);
        console.log("- Bike.allBikes OK!");
        done();
      });
    });
  });

  describe("Bike.add", () => {
    it("se agregó una bike", (done) => {
      var bike = new Bike({ code: 1, color: "verde", model: "montaña" });
      //var bike = Bike.createInstance(1, "verde", "montaña", [-34.5, -54.9]);

      Bike.add(bike, (err, bike) => {
        Bike.allBikes((err, bikes) => {
          expect(bikes.length).toBe(1);
          expect(bikes[0].code).toEqual(bike.code);
          console.log("- Bike.add OK!");
          done();
        });
      });
    });
  });

  describe("Bike.findByCode", () => {
    it("devuelvo la bici con code 1", (done) => {
      Bike.allBikes((err, bikes) => {
        expect(bikes.length).toEqual(0);
      });

      var bike = new Bike({ code: 1, color: "verde", model: "montaña" });
      Bike.add(bike, (err, newBike) => {
        if (err) console.log(err);

        var bike2 = new Bike({ code: 2, color: "rojo", model: "urbana" });
        Bike.add(bike2, (err, newBike) => {
          if (err) console.log(err);

          Bike.findByCode(1, (err, targetBike) => {
            expect(targetBike.code).toBe(bike.code);
            expect(targetBike.color).toBe(bike.color);
            expect(targetBike.model).toBe(bike.model);
            console.log("- Bike.findByCode OK!");
            done();
          });
        });
      });
    });
  });

  describe("Bike.removeByCode", () => {
    it("elimina la bici con code 2", (done) => {
      Bike.allBikes((err, bikes) => {
        expect(bikes.length).toEqual(0);
      });

      var bike = new Bike({ code: 1, color: "verde", model: "montaña" });
      Bike.add(bike, (err, newBike) => {
        if (err) console.log(err);

        var bike2 = new Bike({ code: 2, color: "rojo", model: "urbana" });
        Bike.add(bike2, (err, newBike) => {
          if (err) {
            console.log(err);
          }

          Bike.removeByCode(2, (err, delBike) => {
            expect(delBike.ok).toBe(1);
            expect(delBike.deletedCount).toEqual(1);
            console.log("- Bike.removeByCode");
            done();
          });
        });
      });
    });
  });
});

/*

// se ejecuta antes de cada test/describe
beforeEach(() => {
  Bike.allBikes = [];
});


 describe : funcion que agrupa especificaciones/pruebas relacionadas
 it : define cada uno de los casos de pruebas, evalua casa prueba en particulas
 expect : verifica comportamiento del codigo con lo que esperamos como resultado
    toBe, toEqual, toBeThuthy(), toBeNull()


describe("Bike.allBikes", () => {
  it("comienza vacia", () => {
    expect(Bike.allBikes.length).toBe(0);
  });
});

describe("Bike.add", () => {
  it("agregando una", () => {
    console.log(" Agrega un bici");
    expect(Bike.allBikes.length).toBe(0);
    var a = new Bike(1, "rojo", "urban", [-38.017059, -57.7406706]);
    Bike.add(a);
    expect(Bike.allBikes.length).toBe(1);
    expect(Bike.allBikes[0]).toBe(a);
  });
});

describe("Bike.findById", () => {
  it("debe devolver la bici con id 1", () => {
    console.log(" Busca bici por id");
    expect(Bike.allBikes.length).toBe(0);
    var a = new Bike(1, "verde", "urban", [-38.017059, -57.7406706]);
    var b = new Bike(2, "azul", "urban", [-38.017059, -57.7406706]);
    Bike.add(a);
    Bike.add(b);

    var tarjetBike = Bike.findById(1);
    expect(tarjetBike.id).toBe(1);
    expect(tarjetBike.color).toBe(a.color);
    expect(tarjetBike.model).toBe(a.model);
  });
});

describe("Bike.removeById", () => {
  it("debe eliminar una bicicleta", () => {
    console.log(" Elimina bici por id");
    expect(Bike.allBikes.length).toBe(0);

    var a = new Bike(1, "verde", "urban", [-38.017059, -57.7406706]);
    var b = new Bike(2, "azul", "urban", [-38.017059, -57.7406706]);
    Bike.add(a);
    Bike.add(b);

    var size = Bike.allBikes.length;
    console.log("Antes de Eliminar hay: " + size);
    for (var i = 0; i < size; i++) {
      if (Bike.allBikes[i].id == 2) {
        Bike.allBikes.splice(i, 1);
        break;
      }
    }
    console.log("Despues de Elimnar hay: " + Bike.allBikes.length);
    expect(Bike.allBikes.length).toBe(size - 1);
  });
});
*/
