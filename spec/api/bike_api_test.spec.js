var Bike = require("../../models/bike");
var request = require("request");
var server = require("../../bin/www"); // se ejecuta
var mongoose = require("mongoose");

var base_url = "http://localhost:3000/api/bikes";

describe("Bikes API", () => {
  beforeAll((done) => {
    mongoose.connection.close(done);
  });

  // nos conectamos a la db
  beforeEach((done) => {
    var mongoDB = "mongodb://localhost/testdb";
    mongoose.connect(mongoDB, { useNewUrlParser: true });
    var db = mongoose.connection;
    db.on("error", console.error.bind(console, "MongoDB connection error:"));

    db.once("open", () => {
      console.log("We are connected to test db");
      done();
    });
  });

  // eliminamos todos
  afterEach((done) => {
    Bike.deleteMany({}, (err, success) => {
      if (err) console.log(err);

      done();
      // cierro la conexion sino tira error de timeout
      mongoose.disconnect();
    });
  });

  describe("GET BIKES /", () => {
    it("Status 200", (done) => {
      request.get(base_url, (err, response, body) => {
        var result = JSON.parse(body);

        expect(response.statusCode).toBe(200);

        expect(result.bikes.length).toBe(0);
        done();
      });
    });
  });

  describe("POST BIKES /create", () => {
    it("Status 200", (done) => {
      //console.log("Post bikes /create ");
      var url = base_url + "/create";
      var headers = { "content-type": "application/json" };

      var body =
        '{"code":5,"color":"verde","model":"urbano","lat":-38.017059,"lng":-57.7406706}';

      /* var body = JSON.stringify({
        code: 5,
        //color: "verde",
        //model: "urban",
        //lat: -38.017059,
        //lng: -57.7406706,
      });
*/
      request.post({ headers, url, body }, (err, response, body) => {
        expect(response.statusCode).toBe(200);
        var bike = JSON.parse(body);

        expect(bike.color).toBe("verde");
        expect(bike.location[0]).toBe(-38.017059);
        expect(bike.location[1]).toBe(-57.7406706);
        done(); // espera jasmine para finalizar el test
      });
    });
  });

  describe("PUT BIKES /update", () => {
    it("Status 200", (done) => {
      const a = new Bike({
        code: 2,
        color: "rojo",
        modelo: "montaña",
      });
      a.location = [-38.017059, -57.7406706];

      a.save((err, bike) => {
        var bikeUpdate =
          '{"code":2,"color":"verde","model":"urban","lat":-38.017059,"lng":-57.7406706}';

        var url = base_url + "/update";
        var headers = { "content-type": "application/json" };

        request.put(
          { headers, url, body: bikeUpdate },
          (err, response, body) => {
            if (err) console.log(err);
            expect(response.statusCode).toBe(203);

            Bike.findByCode(2, (err, theBike) => {
              expect(theBike.color).toBe("verde");
              done(); // espera jasmine para finalizar el test
            });
          }
        );
      });
    });
  });

  describe("DELETE BIKES /delete", () => {
    // creo un a bici y borro la bici creada
    it("Status 204", (done) => {
      const a = new Bike({
        code: 7,
        color: "rojo",
        modelo: "montaña",
      });
      a.location = [-38.017059, -57.7406706];

      a.save(() => {
        var url = base_url + "/delete";

        request.delete({ url, code: 7 }, (err, response, result) => {
          expect(response.statusCode).toBe(204);
          Bike.allBikes((err, newBikes) => {
            expect(newBikes.length).toBe(1);
          });
          done(); // espera jasmine para finalizar el test
        });
      });
    });
  });
});

/* ANTES SIN PERSISTENCIA 

beforeEach(() => {
  Bike.allBikes = [];
});

describe("Bikes API", () => {
  describe("GET BIKES /", () => {
    it("Status 200", () => {
      expect(Bike.allBikes.length).toBe(0);
      //console.log("Get bikes / ");
      var a = new Bike(1, "verde", "urban", [-38.017059, -57.7406706]);
      var b = new Bike(2, "azul", "urban", [-38.017059, -57.7406706]);
      Bike.add(a);
      Bike.add(b);

      request.get(base_url, (err, response, body) => {
        expect(response.statusCode).toBe(200);
      });
    });
  });

  describe("POST BIKES /create", () => {
    it("Status 200", (done) => {
      //console.log("Post bikes /create ");
      var url = "http://localhost:3000/api/bikes/create";
      var headers = { "content-type": "application/json" };
      var body = JSON.stringify({
        id: 5,
        color: "verde",
        model: "urban",
        lat: -38.017059,
        lng: -57.7406706,
      });

      request.post({ headers, url, body }, (err, response, body) => {
        expect(response.statusCode).toBe(200);
        expect(Bike.findById(5).color).toBe("verde");
        done(); // espera jasmine para finalizar el test
      });
    });
  });

  describe("PUT BIKES /update", () => {
    it("Status 200", (done) => {
      var b = new Bike(2, "azul", "urban", [-38.017059, -57.7406706]);
      Bike.add(b);

      var bike = Bike.findById(2);
      bike.color = "verde";
      bike.model = "montain";
      bike.location = bike.location;

      var url = "http://localhost:3000/api/bikes/update";
      var headers = { "content-type": "application/json" };
      var body = JSON.stringify(bike);

      request.put({ headers, url, body }, (err, response, body) => {
        expect(response.statusCode).toBe(200);
        expect(Bike.findById(2).color).toBe("verde");
        done(); // espera jasmine para finalizar el test
      });
    });
  });

  describe("DELETE BIKES /delete", () => {
    // creo una bici
    it("Status 200", (done) => {
      var url = "http://localhost:3000/api/bikes/create";
      var headers = { "content-type": "application/json" };
      var body = JSON.stringify({
        id: 5,
        color: "verde",
        model: "urban",
        lat: -38.017059,
        lng: -57.7406706,
      });

      request.post({ headers, url, body }, (err, response, body) => {
        expect(response.statusCode).toBe(200);
        expect(Bike.findById(5).color).toBe("verde");
        done(); // espera jasmine para finalizar el test
      });
    });

    // borro la bici creada
    it("Status 204", (done) => {
      //console.log("Post bikes /create ");
      var url = "http://localhost:3000/api/bikes/delete";

      request.delete({ url, id: 5 }, (err, response, body) => {
        expect(response.statusCode).toBe(204);
        expect(Bike.allBikes.length).toBe(0);
        done(); // espera jasmine para finalizar el test
      });
    });
  });
});
*/
