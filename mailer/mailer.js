const nodemailer = require("nodemailer");
const sgTransport = require("nodemailer-sendgrid-transport");

let mailConfig;

if (process.env.NODE_ENV === "production") {
  const options = {
    auth: {
      api_key: process.env.SENDGRID_API_SECRET,
    },
  };
  mailConfig = sgTransport(options);
} else {
  if (process.env.NODE_ENV === "staging") {
    console.log("XXXXXX");
    const options = {
      auth: {
        api_key: process.env.SENDGRID_API_SECRET,
      },
    };
    mailConfig = sgTransport(options);
  } else {
    mailConfig = {
      host: "smtp.ethereal.email",
      port: 587,
      //secure: true,
      auth: {
        user: process.env.ethereal_user,
        pass: process.env.ethereal_pass,
      },
    };
  }
}
module.exports = nodemailer.createTransport(mailConfig);

/*
// Message object
let message = {
  from: "Sender Name <sender@example.com>",
  to: "Recipient <recipient@example.com>",
  subject: "Nodemailer is unicode friendly ✔",
  text: "Hello to myself!",
  html: "<p><b>Hello</b> to myself!</p>",
};

mailConfig.sendMail(message, (err, info) => {
  if (err) {
    console.log("Error occurred. " + err.message);
    return process.exit(1);
  }

  console.log("Message sent: %s", info.messageId);
  // Preview only available when sending through an Ethereal account
  console.log("Preview URL: %s", nodemailer.getTestMessageUrl(info));
});
*/

/*

Name	Sonny Hermiston
Username	sonny32@ethereal.email (also works as a real inbound email address)
Password	bY1b1meWkqdCGwuGDt

*/
